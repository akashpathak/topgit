var topgit = angular.module('topgit', ['ngRoute']);

topgit.config(function($routeProvider) {
    $routeProvider
        .when('/index', {
            controller: 'usersController',
            templateUrl: 'app/templates/usersList.html'
        })
        .otherwise({
            redirectTo: '/index'
        });
});
